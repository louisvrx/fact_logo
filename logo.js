// UNITS
let padding = [2,2]; // units
let logo_dimensions = [80, 22]; // units
let box_dimensions = [4, 1.65] // units
// PIXELS
let tile_size // pixels
let box_size // pixels

// INTERACTIVTY
let modes = {
  MOUSE_DIR: 'mouse_dir',
  ALL_DIR: 'all_dir',
  NOISE: 'noise',
  MOUSE_NOISE: 'mouse_noise',
  FIXE: 'fixe',
  SIN: 'sin',
  SINX: 'sinx',
  SINY: 'siny',
}

let mode = modes.SINY;
let zoff = 0;
let zinc = 0.005;

// DEBUG
let grid = false;



let letter_positions = [
  // F
  [4,3], [8,3], [12,3], [16, 3],
  [4,7],
  [4,11], [8,11], [12,11],
  [4,15],
  [4,19],
  // //A
  [26, 3],
  [24, 7], [28, 7],
  [22,11], [30,11],
  [20,15], [24,15], [28,15], [32,15],
  [18,19], [34,19],
  // //C
  [48,2], 
  [44, 4], [52, 4],
  [41,7], [55,7],
  [39,11],
  [41, 15], [55, 15],
  [44, 18], [52, 18],
  [48,20],
  // //T
  [60,3], [64,3], [68,3], [72,3], [76,3],
  [68,7],
  [68, 11],
  [68, 15],
  [68,19]
]


/////////////////////
//     SETUP       //
/////////////////////
function setup() {
  console.log('LOGO!!!')
  setup_canvas()

  usage = createDiv('<b>Keys</b><br> <b>Modes</b><br> q: mouse dir<br>s : All dir<br>d: Noise<br>f : Fixe<br>g: Noise Mouse<br>h: Sinus<br>j: Sinus X<br>k: Sinus Y<br><br>/// DEBUG ///<br>z : degrade FPS<br>w : debug grid<br>a: Save Frame<br>')
  usage.style('padding', padding[0]*tile_size+'px '+ padding[1]*tile_size+'px')
  usage.style('font-size', '1em')
  
}

function setup_canvas() {

  div_width = document.getElementById("logo_canvas").offsetWidth
  tile_size = div_width / (logo_dimensions[0]+padding[1]);
  box_size = [floor(tile_size*box_dimensions[0]), floor(tile_size*box_dimensions[1])]
  let canvas = createCanvas(div_width, tile_size*logo_dimensions[1]+padding[1]*tile_size, P2D)
  canvas.parent("logo_canvas")
}

function windowResized() {
  setup_canvas()
}


////////////////////
//      DRAW      //
////////////////////
function draw() {
  // background(220);
  clear()
  translate(padding[0]*tile_size/2, padding[1]*tile_size/2)
  if (grid)  debug_grid();
  draw_logo();
  zoff += zinc;

}

function draw_logo() {
  rectMode(CENTER);
  letter_positions.forEach(function(letter_position) {
    push();
    
    let x = letter_position[0]*tile_size ;
    let y = letter_position[1]*tile_size;
    translate(x, y);
    fill(0);

    let angle = get_angle(x, y) ;
    rotate(angle);  
  
    rect(0, 0, box_size[0], box_size[1]);
    pop();
  }
  ) 

}


/////////////////////////
//       MODES         //
/////////////////////////
function get_angle(x, y ) {
  if (mode == modes.MOUSE_DIR) {
    return mouse_dir(x, y)
  } else if (mode == modes.ALL_DIR) {
    return mouse_all(x, y)
  } else if (mode == modes.NOISE) {
    return noise_dir(x,y)
  } else if (mode == modes.FIXE) {
    return 0
  } else if (mode == modes.MOUSE_NOISE) {
    return mouse_noise_dir(x, y)
  }  else if (mode == modes.SIN) {
    return sin_dir(x, y)
  }  else if (mode == modes.SINX) {
    return sin_x_dir(x, y)
  }  else if (mode == modes.SINY) {
    return sin_y_dir(x, y)
  }
  return 0   
}


function mouse_dir(x, y) {
  return atan2( mouseY-y-padding[1]/2*tile_size, mouseX-x-padding[0]/2*tile_size)
}

function noise_dir(x, y) {
  let value = noise(x, y, zoff) * TWO_PI;
  return value
  
}

function mouse_noise_dir(x, y) {
  let z = map(mouseX, 0, windowWidth, 0, 1)
  let value = noise(x, y, z+zinc) * TWO_PI;
  return value
}

function mouse_all(x, y) {
  return map(mouseX,  - windowWidth/2 + width/2 , windowWidth/2 + width/2, -PI/2, PI/2)
}

function sin_dir(x, y) {
  let dx = 0.025
  let d = dist(x, y, width/2, height/2)
  let mousex_norm = map(mouseX, -windowWidth/2 + width/2 , windowWidth/2 + width/2, 1, -1)
  let angle = sin(d*dx*mousex_norm) + mousex_norm*PI
  return angle
}

function sin_y_dir(x, y) {
  let value =  map( y/height, 0, 1, -PI/2, PI/2)
  let mousex_norm = map(mouseX, -windowWidth/2 + width/2 , windowWidth/2 + width/2, 1, -1)
  let angle_value = sin(value*mousex_norm) + mousex_norm*PI
  return angle_value
}

function sin_x_dir(x, y) {
  let value = map(x/ width, 0, 1, 0, TWO_PI)
  let mousex_norm = map(mouseX, -windowWidth/2 + width/2 , windowWidth/2 + width/2, 1, -1)
  let angle_value = sin(value*mousex_norm) + mousex_norm*PI
  return angle_value
}


/////////////////////
//      DEBUG      //
/////////////////////

function debug_grid() {
  
  // horizontal
    for (ty=0; ty <= logo_dimensions[1]; ty++) {
        line(0, ty*tile_size, logo_dimensions[0]*tile_size ,ty*tile_size)
    }
  // vertical
    for (tx=0; tx <= logo_dimensions[0]; tx++) {
      line(tx*tile_size, 0, tx*tile_size , logo_dimensions[1]*tile_size)          
    }
}

///////////////////////
//       KEYS        //
///////////////////////
function keyPressed() {
    if (key === 'w') {
      grid = !grid
    } else if (key === 'x') {
      rotation = !rotation
    } else if (key == 'q') {
        mode = modes.MOUSE_DIR
    } else if (key == 's') {
      mode = modes.ALL_DIR
    } else if (key == 'd') {
      mode = modes.NOISE
    } else if (key == 'f') {
      mode = modes.FIXE
    } else if (key == 'g') {
      mode = modes.MOUSE_NOISE
    } else if (key == 'h') {
      mode = modes.SIN
    }  else if (key == 'j') {
      mode = modes.SINX
    }  else if (key == 'k') {
      mode = modes.SINY
    } else if (key == 'a') {
      saveCanvas('fact', 'png')
    }

}

//////////

degradeFPS = function(enable) {
  if (enable) frameRate(10);
  else {frameRate(100)}

}