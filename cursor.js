// Basic Pursue


function cursor_sketch(p) {
    p.disableFriendlyErrors = true

    let parent_canvas = 'cursor'

    let size_cursor = 10
    let size_around = 30

    let position_delay = 3 ;
    let positions = [];
    let idx = 0;
    var low_fps = false;
  
    p.setup = function() {
        let canvas = p.createCanvas(p.windowWidth, p.windowHeight , p.P2D)
        canvas.parent(parent_canvas)
        p.noCursor();
    };

    p.windowResized = function() {
        let canvas = p.createCanvas(p.windowWidth, p.windowHeight , p.P2D)
        canvas.parent(parent_canvas)
      }
      
    
    p.draw = function() {
        p.clear()
        p.basicPursue();

    };

    p.keyPressed = function() {
      low_fps = !low_fps
        console.log('DEGRADE FPS : '+low_fps)
        if (p.key == 'z') p.degradeFPS(low_fps)
    }


    p.degradeFPS = function(enable) {
      
      if (enable) p.frameRate(10);
      else {p.frameRate(100)}

    }

    p.basicPursue = function() {
      p.fill(0);
      p.ellipse(p.mouseX, p.mouseY, size_cursor)

      current_position = [p.mouseX, p.mouseY]
      if (positions[idx] != current_position) {
        positions[idx] = current_position
        idx = (idx+1) % position_delay
      }

      p.noFill();
      p.strokeWeight(2);
      let old_position = positions[idx]
      if (old_position === undefined) {
          old_position = current_position
      }
      p.ellipse(old_position[0], old_position[1], size_around)


  }   

}
